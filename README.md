# README #

Allows users to accurately measure the round trip delay of a configurable amount of messages and then have it automatically aggregated providing average latency, standard deviation of latency, and average throughput. 


### Dependencies ###

* MPI: tested on version 3.1.1 of MPICH

### How to build ###
* mpicc -/path/to/pingpong.c -lm -o /path/to/pingpong

### How to run ###
* Sample run on 8 processes 
* * mpiexec -n 8 -f /path/to/machinefile ./examples/pingpong 8 0
* First parameter gives the number of mpi_ranks
* Second parameter decides whether to print overall statistics at end using MPI_Reduce calls