#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "rdtsc.h"
#include <stdio.h>
#include <math.h>
#define ClockFrequency 3000000000.0
int main (int argc, char** argv){
	const int LIMIT= 1000;
	MPI_Init(NULL,NULL);
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&world_rank);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD,&world_size);
	if (world_size <2){
		printf("Less than two \n");
		MPI_Abort(MPI_COMM_WORLD,1);
	}
	int count[256];
	count[0]=0;
	int partner_rank_send= (world_rank +1)%atoi(argv[1]);
	int partner_rank_recv = world_rank -1;
	if (world_rank==0) partner_rank_recv =  atoi(argv[1])-1;
	unsigned long long  start[10000];
	double sum1;
	unsigned long long  end[10000];
	unsigned long long through[10000];
//	unsigned long long total[10000000];
	int counter = 0;
	int temp = 0;
//	MPI_Barrier(MPI_COMM_WORLD);
//	if (partner_rank_send ==1 || partner_rank_recv==0){
	while ( counter < LIMIT){
		if (world_rank %2 == counter% 2 ){
//		if (world_rank == 0 ){
			start[counter] = rdtsc();
			MPI_Send(count, 256,MPI_INT,partner_rank_send,0,MPI_COMM_WORLD);
			MPI_Recv(count,256,MPI_INT,partner_rank_send,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			end[counter]=rdtsc();
//			printf("\nStart node: %d End Node: %d Start time: %f End Time: %f Duration: %f \n",world_rank,partner_rank_send,1000000*(start[counter]/ClockFrequency),1000000*(end[counter]/ClockFrequency),1000000*((end[counter]-start[counter])/ClockFrequency));
//			printf("%f\n" , 1000000*((end[counter]-start[counter])/ClockFrequency));
			through[counter]=0;
			if (end[counter]-start[counter] != 0) {
				sum1+= (ClockFrequency* .001024)/(end[counter]-start[counter]);//printf("%f\n", through[counter]);
			}
			counter++;
			temp++;
		}
		else {
			MPI_Recv(count, 256,MPI_INT,partner_rank_recv,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			MPI_Send(count,256,MPI_INT,partner_rank_recv,0,MPI_COMM_WORLD);
			counter++;
			temp++;
		}
	}
//	}
	int i = 0;
	double  sum = 0;
	int num_values=0;
	for ( i=10; i < counter; i++){
//		double temp=0;
		if(end[i]-start[i] !=0){
		double temp = ((end[i]-start[i]))/ClockFrequency;
//		printf("%f\n",temp);
		sum+=temp;
		num_values++;
	}
		
//		if (world_rank==0)
//		printf("\n %f \n",temp);
		
	}
	double avg = 0;
	if (counter!= 0)
	 avg = (1000000*sum)/(num_values);
	double total_avg ;
	i =0;
	double std=0;
	for (i=10; i< counter; i++){
		double temp = 0;
		 if (end[i]-start[i] != 0)
		 temp = ((1000000 * (end[i]-start[i]))/ClockFrequency) - avg;
		temp = temp * temp;
		std += temp;
	}
	if (counter!=0)
	std = std/num_values;
	std= pow(std,.5);
	printf("my std %f \n \n", std);
	double total_std;
	i = 10;
	
	for (; i<counter; i ++){
		if(through[counter]!=0){
			sum1+=through[counter];
			printf("%llu sum \n ",through[counter]);
		}
	}
	double through_avg = sum1/num_values;
	double through_total;
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Reduce(&through_avg,&through_total,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	MPI_Reduce(&std,&total_std,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	MPI_Reduce(&avg, &total_avg,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	if (world_rank==0){ 
		if (atoi(argv[2]) != 1){
		printf("\n\n\n %f", total_avg/atoi(argv[1]));
		printf("\n std  %f\n",total_std/atoi(argv[1]));
		printf("\n throughput %f\n",through_total/atoi(argv[1]));
		}
		
	}
	MPI_Finalize();
}
